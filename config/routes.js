/**
* Route Mappings
* (sails.config.routes)
*
* Your routes map URLs to views and controllers.
*
* If Sails receives a URL that doesn't match any of the routes below,
* it will check for matching files (images, scripts, stylesheets, etc.)
* in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
* might match an image file: `/assets/images/foo.jpg`
*
* Finally, if those don't match either, the default 404 handler is triggered.
* See `api/responses/notFound.js` to adjust your app's 404 logic.
*
* Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
* flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
* CoffeeScript for the front-end.
*
* For more information on configuring custom routes, check out:
* http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
*/

module.exports.routes = {

    '/': {view: 'app'},

    'GET /j5/led/on/:number' : 'J5/ledController.on',
    'GET /j5/led/off/:number' : 'J5/ledController.off',
    'GET /j5/led/fade/:number/:brillo' : 'J5/ledController.fade',

    'GET /j5/servo/open' : 'J5/servoController.open',
    'GET /j5/servo/close' : 'J5/servoController.close',
    'GET /j5/servo/to/:angle' : 'J5/servoController.to',

};
