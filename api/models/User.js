/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

 var bcrypt = require('bcryptjs');

 module.exports = {

 	attributes : {
 		name : {
 			type : 'string',
 			required : true,
 		},
 		lastname : {
 			type : 'string',
 			defaultsTo : '',
 		},
 		username : {
 			type: 'string',
 			unique : true,
 			required : true,
 		},
 		password : {
 			type : 'string',
 			minLength : 6,
 			required : true,
 		},
 	},

 	beforeCreate: function (values, cb) {
 		bcrypt.hash(values.password, 8, function(err, hash) {
 			if(err) return cb(err);
 			values.password = hash;
 			cb();
 		});
 	},

};
