/**
 * J5/servoController
 *
 * @description :: Server-side logic for managing j5/servoes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	index : function(req, res){
		var data = sails.board.servo.sendData();
		
		res.json({status: true});
	},

	open : function(req, res){
		sails.board.servo.open();
		res.json({status: true});
	},

	close : function(req, res){
		sails.board.servo.close();
		res.json({status: true});
	},

	to : function(req, res){
		var angle = parseInt(req.params.angle);

		if(!isNaN(angle))
			sails.board.servo.to(angle);

		res.json({status: !isNaN(angle)});
	},
	
};
