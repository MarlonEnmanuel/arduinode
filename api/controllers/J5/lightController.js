/**
 * J5/lightController
 *
 * @description :: Server-side logic for managing j5/lights
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	index : function(req, res) {

		sails.board.light.sendData();

	}

};

