/**
 * J5/proximityController
 *
 * @description :: Server-side logic for managing j5/proximities
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	index : function(req, res) {

		sails.board.proximity.sendData();

	}
	
};

