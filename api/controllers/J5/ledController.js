/**
 * J5/ledController
 *
 * @description :: Server-side logic for managing j5/leds
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	index : function(req, res){
		var data = sails.board.leds.sendData();
		
		res.json({status: true});
	},

	on : function(req, res) {
		var led = parseInt(req.params.number);

		if(!isNaN(led))
			sails.board.leds.on(led);

		res.json({status: !isNaN(led)});
	},

	off : function(req, res) {
		var led = parseInt(req.params.number);

		if(!isNaN(led))
			sails.board.leds.off(led);

		res.json({status: !isNaN(led)});
	},

	fade : function(req, res) {
		var led = parseInt(req.params.number);
		var brillo = parseInt(req.params.brillo);

		if(!isNaN(led) && !isNaN(brillo))
			sails.board.leds.fade(led, brillo);

		res.json({status: !isNaN(led) && !isNaN(brillo)});
	}
	
};

