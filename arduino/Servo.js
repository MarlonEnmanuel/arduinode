
var five = require('johnny-five');

module.exports = {

	init : function(board){

		this.board = board;

		this.device = new five.Servo({
			pin: 7,
			center: true
		});

		this.value = 0;
		this.device.to(0+10);

		return this;
	},

	sendData : function(user){
		sails.sockets.blast('j5-servo', {
			status : this.board.status,
			user : user,
			value : this.value,
			name : 'Puerta',
		});
	},

	open : function(user){
		this.to(90, user);
	},

	close: function(user){
		this.to(0, user);
	},

	to: function(angle, user){
		angle = five.Fn.constrain(angle, 0, 90);

		this.device.to(angle+10);

		this.value = angle;

		this.sendData(user);
	},

}