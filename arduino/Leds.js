
var five = require('johnny-five');

module.exports = {

	init : function(board){
		this.board = board;

		this.led1 = new five.Led(6);
		this.led2 = new five.Led(5);

		this.value1 = 0;
		this.value2 = 0;
		
		return this;
	},

	sendData : function(user) {
		sails.sockets.blast('j5-leds', {
			status : this.board.status,
			user : user,
			value : {
				led1 : this.value1,
				led2 : this.value2,
			},
			name : 'Iluminación',
		});
	},

	on : function(number, user){
		this.fade(number, 255, user);
	},

	off : function(number, user){
		this.fade(number, 0, user);
	},

	fade : function(number, brillo, user){
		number = five.Fn.constrain(number, 1, 2);
		brillo = five.Fn.constrain(brillo, 0, 255);

		var iled = 'led'+number;
		var ival = 'value'+number;

		this[ival] = brillo;
		this[iled].fade(brillo, 1000);

		this.sendData(user);
	},

}