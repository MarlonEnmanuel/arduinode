var five = require('johnny-five');
var EtherPort = require("etherport");

var Leds = require('./Leds.js');
var Servo = require('./Servo.js');

var Light = require('./Light.js');
var Proximity = require('./Proximity.js');

module.exports = {

	init : function(){
		this.connect();
	},

	connect : function(){
		var self = this;

		console.log('Iniciando Arduino ...');

		this.board = new five.Board({
			port: new EtherPort(3030),
			repl: false,
			debug: true,
		});

		this.board.on('ready', function(){
			self.status = true;
			console.log('Conectado a Arduino!');

			self.leds = Leds.init(self);
			self.servo = Servo.init(self);
			self.light = Light.init(self);
			self.proximity = Proximity.init(self);
		});

		this.board.on('error', function(err){
			self.status = false;
			console.log('Error al conectar a arduino : '+err);
		});
	},

}