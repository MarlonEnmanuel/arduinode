
var five = require('johnny-five');

module.exports = {

	init : function(board){

		this.board = board;
		
		this.sensor = five.Light("A0");

		this.sensor.on('change', this.sendData.bind(this));

		return this;
	},

	sendData : function(user){
		sails.sockets.blast('j5-light', {
			status : this.board.status,
			user : user,
			value : this.sensor.level,
			name : 'Sensor de Luz',
		});
	},

}