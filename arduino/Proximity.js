
var five = require('johnny-five');

module.exports = {

	init : function(board){

		this.board = board;

		this.sensor = new five.Proximity({
			controller: "HCSR04I2CBACKPACK",
		});

		this.sensor.on("change", this.sendData.bind(this));

		return this;
	},

	sendData : function(user){
		sails.sockets.blast('j5-proximity', {
			status : this.board.status,
			user : user,
			value : this.sensor.cm,
			name : 'Sensor de Distancia',
		});
	},

}