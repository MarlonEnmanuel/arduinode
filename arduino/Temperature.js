
var five = require('johnny-five');

module.exports = {

	init : function(board){

		this.board = board;

		this.sensor = new five.Thermometer({
			controller : "DHT22_I2C_NANO_BACKPACK",
		});

		this.sensor.on("change", this.sendData.bind(this));
		
		return this;
	},

	sendData : function(user){
		sails.sockets.blast('j5-light', {
			status : this.board.status,
			user : user,
			value : {
				C : this.sensor.C,
				F : this.sensor.F,
				K : this.sensor.K
			},
			name : 'Sensor de Temperatura',
		});
	},

}